import { createServer } from './app/server';
import { PORT } from './config/server';

createServer().then((server) => {
  server.listen(PORT, () => {
    console.info(`\n`);
    console.info(`----------- LAUNCHING SERVER -----------`);
    console.info(`\n`);
    console.info(`Server is listening on port ${PORT}`);
    console.info(`\n`);
    console.info(`----------------------------------------`);
  });
});
